# MeditorJS

## v1.0.1

- Fixed editor don't returning to his original height when exiting fullscreen
- Removed strange borders on the left and the bottom of the editor.

## v1.0.0

First version of MeditorJS fork of [Editor.md](https://github.com/Pandao/Editor.md)
- better dark theme for preview and menu
- fixed github emojis
- changed some names from editormd to meditorjs
- minor code improvements